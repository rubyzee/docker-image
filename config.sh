#!/bin/bash

# Git Configuration
export GIT_USERNAME="Egii"
export GIT_EMAIL="regidesoftian@gmail.com"

git config --global user.name "${GIT_USERNAME}"
git config --global user.email "${GIT_EMAIL}"

# TimeZone Configuration
export TZ="Asia/Jakarta"
ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime
